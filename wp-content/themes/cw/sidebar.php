<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?>
	<aside class="widget-area small-12 medium-4 large-4 columns sidebar" role="complementary">
		<?php //get_search_form( true ); ?>
		<br>
		<h4 class="h_blue">Helpful Links</h4>
			<hr class="page_hr">
			<h5 class="sidebar_inner_link"><a href="<?php echo get_site_url(); ?>/how-to-report">How to Report Abuse</a></h5>
			<h5 class="sidebar_inner_link"><a href="<?php echo get_site_url(); ?>/types-of-child-abuse/">Types of Abuse</a></h5>
			<h5 class="sidebar_inner_link"><a href="https://secure.piryx.com/donate/RrlHSjRs/Patsy-s-House-Children-s-Advocacy-Center/">Donate to Patsy's House</a></h5>
			<h5 class="sidebar_inner_link"><a href="<?php echo get_site_url(); ?>/contact-us/">Contact Us</a></h5>
			<!-- <h5 class="sidebar_inner_link"><a href="<?php echo get_site_url(); ?>/">Counseling</a></h5> -->
			<hr class="page_hr">

		<br>
		<h4 class="h_grey">To report abuse call:</h4>
		<h3><a href="tel:18002525400">1-800-252-5400</a></h3>

		<hr class="page_hr">
		<h4 class="h_blue">Upcoming Events</h4>
		<hr class="page_hr">
		<?php

			global $post;
			$all_events = tribe_get_events(array(
			'eventDisplay'=>'upcoming',
			'posts_per_page'=>2
			));

			foreach($all_events as $post) {
			setup_postdata($post);

				//print_r($post);
				//echo $post->post_date.'<br>';
				//echo $post->post_title.'<br>';
				//echo $post->post_content.'<br>';
				?><a href="<? the_permalink(); ?>"><h5 class="h_grey"><? echo $post->post_title; ?></h5></a><?
				echo '<h6>'.tribe_get_start_date( $post->ID ).'</h6>';
				echo'<div class="act_content">';
					echo '<p>'.$post->post_content.'</p>';
				echo'</div>';

				?><a class=""href="<?php the_permalink(); ?>">Read More</a><hr><?
			} 

		?>

		<hr class="page_hr">
		<div id="amznCharityBanner"><script type="text/javascript">(function() {var iFrame = document.createElement('iframe'); iFrame.style.display = 'none'; iFrame.style.border = "none"; iFrame.width = 310; iFrame.height = 256; iFrame.setAttribute && iFrame.setAttribute('scrolling', 'no'); iFrame.setAttribute('frameborder', '0'); setTimeout(function() {var contents = (iFrame.contentWindow) ? iFrame.contentWindow : (iFrame.contentDocument.document) ? iFrame.contentDocument.document : iFrame.contentDocument; contents.document.open(); contents.document.write(decodeURIComponent("%3Cdiv%20id%3D%22amznCharityBannerInner%22%3E%3Ca%20href%3D%22https%3A%2F%2Fsmile.amazon.com%2Fch%2F75-2666677%22%3E%3Cdiv%20class%3D%22text%22%20height%3D%22%22%3E%3Cdiv%20class%3D%22support-wrapper%22%3E%3Cdiv%20class%3D%22support%22%20style%3D%22font-size%3A%2022px%3B%20line-height%3A%2025px%3B%20margin-top%3A%205.5px%3B%20margin-bottom%3A%205.5px%3B%22%3ESupport%20%3Cspan%20id%3D%22charity-name%22%20style%3D%22display%3A%20inline-block%3B%22%3EPatsy's%20House%20Children's%20Advocacy%20Center.%3C%2Fspan%3E%3C%2Fdiv%3E%3C%2Fdiv%3E%3Cp%20class%3D%22when-shop%22%3EWhen%20you%20shop%20at%20%3Cb%3Esmile.amazon.com%2C%3C%2Fb%3E%3C%2Fp%3E%3Cp%20class%3D%22donates%22%3EAmazon%20donates.%3C%2Fp%3E%3C%2Fdiv%3E%3C%2Fa%3E%3C%2Fdiv%3E%3Cstyle%3E%23amznCharityBannerInner%7Bbackground-image%3Aurl(https%3A%2F%2Fimages-na.ssl-images-amazon.com%2Fimages%2FG%2F01%2Fx-locale%2Fpaladin%2Fcharitycentral%2Fbanner-background-image._CB309675353_.png)%3Bwidth%3A300px%3Bheight%3A250px%3Bposition%3Arelative%7D%23amznCharityBannerInner%20a%7Bdisplay%3Ablock%3Bwidth%3A100%25%3Bheight%3A100%25%3Bposition%3Arelative%3Bcolor%3A%23000%3Btext-decoration%3Anone%7D.text%7Bposition%3Aabsolute%3Btop%3A20px%3Bleft%3A15px%3Bright%3A15px%3Bbottom%3A100px%7D.support-wrapper%7Boverflow%3Ahidden%3Bmax-height%3A86px%7D.support%7Bfont-family%3AArial%2Csans%3Bfont-weight%3A700%3Bline-height%3A28px%3Bfont-size%3A25px%3Bcolor%3A%23333%3Btext-align%3Acenter%3Bmargin%3A0%3Bpadding%3A0%3Bbackground%3A0%200%7D.when-shop%7Bfont-family%3AArial%2Csans%3Bfont-size%3A15px%3Bfont-weight%3A400%3Bline-height%3A25px%3Bcolor%3A%23333%3Btext-align%3Acenter%3Bmargin%3A0%3Bpadding%3A0%3Bbackground%3A0%200%7D.donates%7Bfont-family%3AArial%2Csans%3Bfont-size%3A15px%3Bfont-weight%3A400%3Bline-height%3A21px%3Bcolor%3A%23333%3Btext-align%3Acenter%3Bmargin%3A0%3Bpadding%3A0%3Bbackground%3A0%200%7D%3C%2Fstyle%3E")); contents.document.close(); iFrame.style.display = 'block';}); document.getElementById('amznCharityBanner').appendChild(iFrame); })(); </script></div>



		<!-- <div class="c_stories">
			<h4>Children's Stories</h4>
			<hr class="page_hr">
			<?php 	
				//adjusting the query
				$args = array(
					'category_name' => 'story',
					'post_type' => 'post',
					'posts_per_page' => 1,
					'order' => DESC
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						
						echo'<div class = "">';
							?><a href="<? the_permalink(); ?>"><h5 class="h_grey"><? echo get_the_title(); ?></h5></a><?
							//echo'<div class="grey_bg_title columns small-12 medium-3 large-3"><h6 class="news_date">Testimonials</h6></div>';
							//echo'<div class="columns small-12 medium-3 large-3"><h6 class="news_date">'. get_the_date(). '</h6><br></div>';
			
							echo'<div class="act_content">';
								the_excerpt();
							echo'</div>';

							?><a class=""href="<?php the_permalink(); ?>">Read More</a><?
						echo'</div><br>';
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>
			<hr class="page_hr">
			<br><a class="" href="<?php echo get_site_url(); ?>/childrens-stories/">Read All Stories</a>
		</div> -->

	</aside>