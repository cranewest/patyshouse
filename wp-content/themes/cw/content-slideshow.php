<?php
	global $post;
	$page_id = $post->ID;
	
	$slideshow_args = array(
		'post_type' => 'slideshows',
		'posts_per_page' => -1,
		'meta_key' => '_cwmb_slideshow_page',
		'meta_value' => $page_id
	);

	$slideshow = new WP_Query($slideshow_args);

	if($slideshow->have_posts()){
		while($slideshow->have_posts()) {
			$slideshow->the_post();
			echo '<div class="cw-slideshow s_slide_shadow">';
			$slides = get_post_meta($post->ID, '_cwmb_slideshow', true);

			if(!empty($slides)) {
				echo '<ul class="styleless cw-slider">';
					foreach ($slides as $slide) {
						$cropped = aq_resize( $slide['image'], 1000, 500, true, true, true );

						echo '<li class="slide s_img_padding">';
							echo '<div class="row s_slide">';
							echo '';
							echo '<div class="small-12 medium-12 large-8 columns "><div class="s_img_padding_outer">';
								if(!empty($slide['link'])) { echo '<a class="s_title" href="'.$slide['link'].'">'; }
									
								if(!empty($slide['image'])) { echo '<img src="'.$cropped.'" alt="" />'; }
								$class = '';
								if(!empty($slide['title']) || !empty($slide['caption'])) {
									$class = 'has-cap';
								}
							echo '</div></div>';
							echo '<div class="small-12 medium-12 large-4 columns s_txt_padding"><div class="s_txt_padding">';
								// echo '<div class="slide-words '.$class.'"><hr class="hr_white">';
							echo '<div class="slide-words '.$class.'">';
									if(!empty($slide['title'])) { echo '<h5 class="s_title">'.$slide['title'].'</h5><hr class="hr_white">'; }
									if(!empty($slide['caption'])) { echo '<h6 class="s_title">'.$slide['caption'].'</h6>'; }
								echo '</div>';
								// echo '<hr class="hr_grey"></div>';
							echo '</div></div>';
							echo '</div>';

							if(!empty($slide['link'])) { echo '</a>'; }
						echo '</li>';
					}
				echo '</ul>';
			}
			echo '</div>';
		}
	}
wp_reset_query(); ?>