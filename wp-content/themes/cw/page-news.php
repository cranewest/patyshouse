<?php
/* Template Name: News Page*/
?>
<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="small-12 medium-8 large-8 columns">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php //get_template_part( 'content', 'page' ); ?>
				<?php // comments_template( '', true ); 
					echo'<h2 class="page_title h_grey">'.get_the_title().'</h2><hr class="page_hr">';
					the_content();
				?>
			<?php endwhile; // end of the loop. ?>
			<?php 	
				//adjusting the query
				$args = array(
					// 'category_name' => 'news',
					'post_type' => 'post',
					'posts_per_page' => -1,
					'order' => DESC
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						
						echo'<div class = "">';
							$n_link = get_post_meta($post->ID, '_cwmb_link', true);
							if($n_link)
							{
								?><a href="<? echo $n_link; ?>"><h5 class="h_grey"><? echo get_the_title(); ?></h5></a><?
							}
							else{
								?><a href="<? the_permalink(); ?>"><h5 class="h_grey"><? echo get_the_title(); ?></h5></a><?
							}
							//echo'<div class="grey_bg_title columns small-12 medium-3 large-3"><h6 class="news_date">Testimonials</h6></div>';
							//echo'<div class="columns small-12 medium-3 large-3"><h6 class="news_date">'. get_the_date(). '</h6><br></div>';
			
							echo'<div class="act_content">';
								the_excerpt();
							echo'</div>';

							if($n_link)
							{
								?><a class=""href="<?php echo $n_link; ?>">Read More</a><?
							}
							else{
								?><a class=""href="<?php the_permalink(); ?>">Read More</a><?
							}
						echo'</div><hr>';
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>