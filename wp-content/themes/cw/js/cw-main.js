jQuery(document).ready(function($){
	// uncomment below to support placeholders in < IE10
	// $('input, textarea').placeholder();
	// for debugging
	$('pre').each(function(){
		$(this).find('.close-pre').click(function(){
			$(this).toggleClass('open');
			$(this).next('.content').slideToggle();
		});
	});
	
	if( !$("html").hasClass("lt-ie9") ) {
		$(document).foundation();
		console.log("Foundation Js loaded");
	}

	$('.cw-slider').bxSlider({
		mode: 'fade',
		controls: false,
		auto: true,
		autoStart: true
	});

	// hide captchas (gravity forms)
	$('.gform_wrapper').click(function(event){
		$(this).find('.gf_captcha').slideDown();
		event.stopPropagation();
	});

	$('html').click(function() {
		$('.gf_captcha').slideUp();
	});
});