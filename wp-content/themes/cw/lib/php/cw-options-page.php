<?php 
/**
* Plugin Name: CW Options Page
* Plugin URI: http://crane-west.com
* Description: Custom contact info page
* Version: 1.2
* Author: Joel Abeyta, Crane | West
* Author URI: http://crane-west.com
* License: GPLv2 or later
 */

// add customizable admin page
/**
 * CMB Theme Options
 * @version 0.1.0
 */
class cw_options_Admin {

/**
 * Option key, and option page slug
 * @var string
 */
private $key = 'cw_options_options';

/**
 * Array of metaboxes/fields
 * @var array
 */
protected $option_metabox = array();

/**
 * Options Page title
 * @var string
 */
protected $title = '';

/**
 * Options Page hook
 * @var string
 */
protected $options_page = '';

/**
 * Constructor
 * @since 0.1.0
 */
public function __construct() {
// Set our title
	$this->title = __( 'Site Options', 'cw_options' );
}

/**
 * Initiate our hooks
 * @since 0.1.0
 */
public function hooks() {
	add_action( 'admin_init', array( $this, 'init' ) );
	add_action( 'admin_menu', array( $this, 'add_options_page' ) );
}

/**
 * Register our setting to WP
 * @since  0.1.0
 */
public function init() {
	register_setting( $this->key, $this->key );
}

/**
 * Add menu options page
 * @since 0.1.0
 */
public function add_options_page() {
	$this->options_page = add_menu_page( $this->title, $this->title, 'read', $this->key, array( $this, 'admin_page_display' ) );
}

/**
 * Admin page markup. Mostly handled by CMB
 * @since  0.1.0
 */
public function admin_page_display() {
	?>
	<div class="wrap cmb_options_page <?php echo $this->key; ?>">
		<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
		<?php cmb2_metabox_form( self::option_fields(), $this->key ); ?>
	</div>
	<?php
}

/**
 * Defines the theme option metabox and field configuration
 * @since  0.1.0
 * @return array
 */
public function option_fields() {

// Only need to initiate the array once per page-load
	if ( ! empty( $this->option_metabox ) ) {
		return $this->option_metabox;
	}

	global $cw_states;

	$prefix = 'cwo_';
	$this->fields = array(
		array(
			'name' => 'Site Logo',
			'id' => $prefix.'logo',
			'type' => 'file',
		),
		array(
			'name' => 'Contact Person Name',
			'id' => $prefix.'name',
			'type' => 'text',
		),
		array(
			'name' => 'Title',
			'id' => $prefix.'title',
			'type' => 'text',
		),
		array(
			'name' => 'Address',
			'id' => $prefix.'address1',
			'type' => 'text',
		),
		array(
			'name' => 'Address 2',
			'id' => $prefix.'address2',
			'type' => 'text',
		),		
		array(
			'name' => 'City',
			'id' => $prefix.'city',
			'type' => 'text',
		),
		array(
			'name' => 'State',
			'id' => $prefix.'state',
			'type' => 'select',
			'options' => $cw_states,
		),
		array(
			'name' => 'Zip',
			'id' => $prefix.'zip',
			'type' => 'text',
		),
		array(
			'name' => 'Phone',
			'id' => $prefix.'phone',
			'type' => 'text',
		),
		array(
			'name' => 'Phone 2',
			'id' => $prefix.'phone2',
			'type' => 'text',
		),
		array(
			'name' => 'Fax',
			'id' => $prefix.'fax',
			'type' => 'text',
		),
		array(
			'name' => 'Email',
			'id' => $prefix.'email',
			'type' => 'text',
		),
		array(
			'name' => 'Facebook',
			'desc' => 'Facebook page url',
			'id' => $prefix.'facebook',
			'type' => 'text_url',
		),
		array(
			'name' => 'Twitter',
			'desc' => 'Twitter page url',
			'id' => $prefix.'twitter',
			'type' => 'text_url',
		),
		array(
			'name' => 'Google Analytics Code',
			'id' => $prefix.'ga',
			'type' => 'text',
		),
	);

	$this->option_metabox = array(
		'id' => 'option_metabox',
		'show_on'=> array( 'key' => 'options-page', 'value' => array( $this->key, ), ),
		'show_names' => true,
		'fields' => $this->fields,
		);

	return $this->option_metabox;
}

/**
 * Public getter method for retrieving protected/private variables
 * @since  0.1.0
 * @param  string  $field Field to retrieve
 * @return mixed  Field value or exception is thrown
 */
public function __get( $field ) {

// Allowed fields to retrieve
	if ( in_array( $field, array( 'key', 'fields', 'title', 'options_page' ), true ) ) {
		return $this->{$field};
	}
	if ( 'option_metabox' === $field ) {
		return $this->option_fields();
	}

	throw new Exception( 'Invalid property: ' . $field );
}

}

// Get it started
$cw_options_Admin = new cw_options_Admin();
$cw_options_Admin->hooks();

/**
 * Wrapper function around cmb_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixedOption value
 */
function cw_options_get_option( $key = '' ) {
	global $cw_options_Admin;
	// return cmb_get_option( $cw_options_Admin->key, $key );
	$cw_options = get_option('cw_options_options');
	return $cw_options[$key];
}