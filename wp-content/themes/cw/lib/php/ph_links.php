<?php
function cw_cpp_ph_links_init() {
    $field_args = array(
        'labels' => array(
            'name' => __( 'Website Links' ),
            'singular_name' => __( 'ph_links' ),
            'add_new' => __( 'Add New Listing' ),
            'add_new_item' => __( 'Add New Listing' ),
            'edit_item' => __( 'Edit Listing' ),
            'new_item' => __( 'Add New Listing' ),
            'view_item' => __( 'View Listing' ),
            'search_items' => __( 'Search Links' ),
            'not_found' => __( 'No listings found' ),
            'not_found_in_trash' => __( 'No listings found in trash' )
        ),
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'rewrite' => true,
        'menu_position' => 20,
        'supports' => array('title')
    );
    register_post_type('ph_links',$field_args);
}
add_action( 'init', 'cw_cpp_ph_links_init' );

