<?php
/**
 * Template Name: Home Page Template
 * Description: Custom home page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	
	<div class="row">
		<?php get_template_part('content', 'slideshow'); ?>
		<div class="medium-8 large-8 columns">
			
		</div>
	</div>

	<div class="row">
		
	</div>

	<div class="row slide_bottom_padding">
		
		<div class="small-12 medium-4 large-4 columns">

			<h4 class="h_blue">Helpful Links</h4>
			<hr class="page_hr">
			<h5 class="sidebar_inner_link"><a href="<?php echo get_site_url(); ?>/how-to-report">How to Report Abuse</a></h5>
			<h5 class="sidebar_inner_link"><a href="<?php echo get_site_url(); ?>/types-of-child-abuse/">Types of Abuse</a></h5>
			<h5 class="sidebar_inner_link"><a href="https://secure.piryx.com/donate/RrlHSjRs/Patsy-s-House-Children-s-Advocacy-Center/">Donate to Patsy's House</a></h5>
			<h5 class="sidebar_inner_link"><a href="<?php echo get_site_url(); ?>/contact-us/">Contact Us</a></h5>
			
			<!-- <h5 class="sidebar_inner_link"><a href="<?php echo get_site_url(); ?>/">Counseling</a></h5> -->
			<hr class="page_hr">
			<br>
			<h4 class="h_grey">To report abuse call:</h4>
			<h3><a href="tel:18002525400">1-800-252-5400</a></h3>

			<?php 	
				//adjusting the query
				$args = array(
					'post_type' => 'promos',
					'posts_per_page' => 1,
					'order' => DESC
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						$link = get_post_meta($post->ID, '_cwmb_promo_link', true);
						$img_src = get_post_meta($post->ID, '_cwmb_promo_image', true);
				
						
						// echo'<div class="small-12 medium-12 large-12 columns promo">';
						if ($link){
							echo'<a target="_blank" href="'.$link.'">'; 
							?><img class="promo" src="<? echo $img_src; ?>"> <?
							echo'</a>';
						} 
						else{
							?><img class="promo" src="<? echo $img_src; ?>"> <?
						}
						// echo'</div>';
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>
			<hr class="page_hr">
			<h4 class="h_blue">Upcoming Events</h4>
			<hr class="page_hr">

			<?php

				global $post;
				$all_events = tribe_get_events(array(
				'eventDisplay'=>'upcoming',
				'posts_per_page'=>2
				));

				foreach($all_events as $post) {
				setup_postdata($post);

					//print_r($post);
					//echo $post->post_date.'<br>';
					//echo $post->post_title.'<br>';
					//echo $post->post_content.'<br>';
					?><a href="<? the_permalink(); ?>"><h5 class="h_grey"><? echo $post->post_title; ?></h5></a><?
					echo '<h6>'.tribe_get_start_date( $post->ID ).'</h6>';
					echo'<div class="act_content">';
						echo '<p>'.$post->post_content.'</p>';
					echo'</div>';

					?><a class=""href="<?php the_permalink(); ?>">Read More</a><hr><?
				} 

			?>

		</div>

		<div class="small-12 medium-4 large-4 columns">
			<h4 class="h_blue">from Facebook</h4>
			<hr class="page_hr">

			<?php echo do_shortcode('[custom-facebook-feed num=3 layout=thumb]');?>


		</div>

		<div class="small-12 medium-4 large-4 columns">
			<h4 class="h_blue">Patsy’s House</h4>
			<hr class="page_hr">
			<?php 	
				//adjusting the query
				$args = array(
					// 'category_name' => 'news',
					'post_type' => 'post',
					'posts_per_page' => 4,
					'order' => DESC
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						
						echo'<div class = "">';
							$n_link = get_post_meta($post->ID, '_cwmb_link', true);
							if($n_link)
							{
								?><a target="_blank" href="<? echo $n_link; ?>"><h5 class="h_grey"><? echo get_the_title(); ?></h5></a><?
							}
							else{
								?><a href="<? the_permalink(); ?>"><h5 class="h_grey"><? echo get_the_title(); ?></h5></a><?
							}
							
							//echo'<div class="grey_bg_title columns small-12 medium-3 large-3"><h6 class="news_date">Testimonials</h6></div>';
							//echo'<div class="columns small-12 medium-3 large-3"><h6 class="news_date">'. get_the_date(). '</h6><br></div>';
			
							echo'<div class="act_content">';
								the_excerpt();
							echo'</div>';

							if($n_link)
							{
								?><a class="" target="_blank" href="<?php echo $n_link; ?>">Read More</a><?
							}
							else{
								?><a class="" href="<?php the_permalink(); ?>">Read More</a><?
							}
						echo'</div><hr class="trans">';
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>
			<a class=""href="<?php echo get_site_url(); ?>/news/">Read All News</a><br><br>
		</div>

	</div>

<?php get_footer(); ?>