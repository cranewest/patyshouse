<?php
/* Template Name: staff Page*/
?>
<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="small-12 medium-8 large-8 columns">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php //get_template_part( 'content', 'page' ); ?>
				<?php // comments_template( '', true ); 
					echo'<h2 class="page_title h_grey">'.get_the_title().'</h2><hr class="page_hr">';
					the_content();
				?>
			<?php endwhile; // end of the loop. ?>
			<!-- <hr class="hr_inv"> -->
			<?php 	
				//adjusting the query
				$args = array(
					'post_type' => 'staff',
					'posts_per_page' => -1,
					'orderby' => 'menu_order'
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						
						$p =  get_post_meta($post->ID, '_cwmb_member_radio', true);
						if ('other' == $p) {
							echo'<div class = "row">';
								?>
								<div class="small-12 medium-6 large-6 columns"><h5 class="h_grey"><? echo get_post_meta($post->ID, '_cwmb_staff_name', true); ?></h5></div>
								<div class="small-12 medium-6 large-6 columns"><h6 class="h_grey"><? echo get_post_meta($post->ID, '_cwmb_staff_title', true); ?></h6></div>
								<?
							echo'</div><hr>';

						}
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>
			<?echo'<div class = "row"><hr><h5 class="center">Board Members</h5><hr></div>';?>
			<?php 	
				//adjusting the query
				$args = array(
					'post_type' => 'staff',
					'posts_per_page' => -1,
					'order' => 'ASC',
			        'orderby' => 'meta_value',
			        'meta_key' => '_cwmb_staff_name',
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();

						
						$p =  get_post_meta($post->ID, '_cwmb_member_radio', true);
						if ('board' == $p) {
							echo'<div class = "row">';
								?>
								<div class="small-12 medium-6 large-6 columns"><h5 class="h_grey"><? echo get_post_meta($post->ID, '_cwmb_staff_name', true); ?></h5></div>
								<div class="small-12 medium-6 large-6 columns"><h6 class="h_grey"><? echo get_post_meta($post->ID, '_cwmb_staff_title', true); ?></h6></div>
								<?
							echo'</div><hr>';

						}
							
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>

		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>