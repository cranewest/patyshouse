<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="small-12 medium-8 large-8 columns">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php //get_template_part( 'content', 'page' ); ?>
				<?php // comments_template( '', true ); 
					echo'<h2 class="page_title h_grey">'.get_the_title().'</h2><hr class="page_hr">';
					the_content();
				?>
			<?php endwhile; // end of the loop. ?>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>