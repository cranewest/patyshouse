<?php
/* Template Name: FAQs Page*/
?>
<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="small-12 medium-8 large-8 columns">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php //get_template_part( 'content', 'page' ); ?>
				<?php // comments_template( '', true ); 
					echo'<h2 class="page_title h_grey">'.get_the_title().'</h2><hr class="page_hr">';
					the_content();
				?>
			<?php endwhile; // end of the loop. ?>
			<!-- <hr class="hr_inv"> -->
			<?php 	
				//adjusting the query
				$args = array(
					'post_type' => 'faqs',
					'posts_per_page' => -1,
					'order' => DESC
				);

				// The Query
				$latest_post = new WP_Query( $args );
				// The Loop
				if ( $latest_post->have_posts() ) 
				{
					while ( $latest_post->have_posts() ) 
					{	
						$latest_post->the_post();
						
						echo'<div class = "">';
						echo'<h5 class="">'.get_the_title().'</h5>';
						echo'<h6 class="h_grey">'.the_content().'</h6>';

						echo'</div><hr>';
						
					}
				} 
				else 
				{
					// no posts do nothing
				}

				/* Restore original Post Data */
				wp_reset_postdata();
			?>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>