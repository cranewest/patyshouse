<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?><!DOCTYPE html>
<!--[if lt IE 7]> <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html style="margin-top: 0!important;" class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html style="margin-top: 0!important;" class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<!-- <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png"/> -->

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">
	<link href='http://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>

	<!-- WP_HEAD() -->
	<?php wp_head(); ?>

	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">
	<![endif]-->

	<!-- facebook plugin code -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>


</head>

<body <?php body_class(); ?>>
	<div class="top_head"></div>
	<header role="banner">
		<div class="row">

			<div class="small-12 medium-6 large-6 columns">
				<?php $logo_url = cw_options_get_option( 'cwo_logo' ); ?>
				<h1 class="logo">
					<a href="/" title="<?php bloginfo( 'name' ); ?>">
						<?php if(!empty($logo_url)) { ?>
							<img src="<?php echo $logo_url; ?>" alt="<?php bloginfo( 'name' ); ?>" />
						<?php } else {
							bloginfo( 'name' );
						} ?>
					</a>
				</h1>
			</div>

			<div class="small-12 medium-6 large-6 columns">
				<div class="small-6 medium-6 large-6 columns">
					<div class="header_address f_right">
						<p class="address">
							<?$patsys_title = str_replace("\'", "'",cw_options_get_option( 'cwo_title' ));?>
							<?echo $patsys_title;?><br>
							<?echo cw_options_get_option( 'cwo_address1' );?><br>
							<?echo cw_options_get_option( 'cwo_city' );?>,
							<?echo cw_options_get_option( 'cwo_state' );?>
							<?echo cw_options_get_option( 'cwo_zip' );?><br>
							<?echo '<a href="tel:'.preg_replace("/[^0-9]/","",cw_options_get_option( 'cwo_phone' )).'">'.cw_options_get_option( 'cwo_phone' ).'</a>';?><br>
							<!-- <a href="tel:19408726284"><strong>1(940)872-6284</strong></a> -->
						</p>
					</div>
				</div>
				<div class="small-6 medium-6 large-6 columns">
					<div class="">
						<a target="_blank" href="https://secure.piryx.com/donate/RrlHSjRs/Patsy-s-House-Children-s-Advocacy-Center/" class="button donate_button ">DONATE</a><br>
						<div class="fb-like" data-href="https://www.facebook.com/pages/Patsys-House-Childrens-Advocacy-Center/208560765825128" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
					</div>
				</div>
				
			</div>

		</div>	

		<div class="nav-container contain-to-grid">
			<nav class="top-bar" data-topbar role="navigation">
				<ul class="title-area">
					<li class="name"></li>
					<li class="toggle-topbar menu-icon"><a href="#">Menu</a></li>
				</ul>

				<section class="top-bar-section">
					<?php 
						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'container' => '',
								'menu_class' => 'menu',
								'depth' => 2,
								'fallback_cb' => 'wp_page_menu',
								'walker' => new Foundation_Walker_Nav_Menu()
							)
						);
					?>
				</section>
			</nav>
		</div>
	</header>