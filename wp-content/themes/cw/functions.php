<?php
/**
 * CW  functions
 *
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */

// for debugging
function echo_pre($input) {
	echo '<pre>';
	echo '<span class="close-pre"></span>';
	echo '<span class="content">';
	print_r($input);
	echo '</span>';
	echo '</pre>';
}

if ( file_exists(__DIR__.'/lib/php/cmb2/init.php')) {
	require_once __DIR__.'/lib/php/cmb2/init.php';
}

/* Include walker for wp_nav_menu */
get_template_part('lib/php/foundation_walker');

// Used for cropping images on the fly.
// Read docs here => https://github.com/syamilmj/Aqua-Resizer/
require_once( 'lib/php/aq_resizer.php' );

/**
 * Sets up theme defaults
 *
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,
 * 	custom background, and post formats.
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since CW 1.0
 */
function cw_setup() {
	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', 'fonts/genericons.css' ) );

	/*
	 * Adds RSS feed links to <head> for posts and comments.
	 */
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Switches default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

	/*
	 * This theme supports all available post formats by default.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
	) );

	/*
	 * Create the main menu location.
	 */
	register_nav_menu( 'primary', __( 'Main Menu', 'cw' ) );

	/*
	 * This theme uses a custom image size for featured images, displayed on "standard" posts.
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop
}
add_action( 'after_setup_theme', 'cw_setup' );

/**
 * Import Custom Post Types, etc.
 *
 *
 * @since CW 1.0
 */
get_template_part('lib/php/cpt-slideshows');
get_template_part('lib/php/cw-options-page');
get_template_part('lib/php/content', 'states');
get_template_part('lib/php/cpt-promos');
get_template_part('lib/php/cpt-staff');
get_template_part('lib/php/cpt-faqs');
get_template_part('lib/php/ph_links');

/**
 * Enqueue front end scripts for CW theme.
 */
add_action("wp_enqueue_scripts", "cw_enqueue_frontend", 11);
function cw_enqueue_frontend() {
	// Places modernizr in head of site
	wp_enqueue_script('modernizr', get_template_directory_uri() .'/js/modernizr.js');

	wp_deregister_script('jquery');
	wp_register_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js", null, '1.10.2', false);

   // Enqueue main.js with jquery and foudnation as a dependency.
	wp_enqueue_script('cw_js', get_template_directory_uri().'/js/main.min.js', array('jquery'), '1', true);

	// Enqueue the threaded comments reply scipt when necessary.
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/* 
	*  Auto-version CSS & JS files, allowing for cache busting when these files are changed.
	*  Avoids using query strings which prevent proxy caching
	*  Adjust paths based on your theme setup. These paths work with Bones theme
	*/
 
	$mtime = filemtime(dirname(__FILE__) . '/css/style.css');
	wp_register_style( 'cw-stylesheet', get_bloginfo('template_url') . '/css/style.css', array(), $mtime, 'all');
	 
	// enqueue the stylesheet
	wp_enqueue_style( 'cw-stylesheet' );
}

/**
 * Creates a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since CW 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function cw_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'cw_wp_title', 10, 2 );

/**
 * Extends the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Active widgets in the sidebar to change the layout and spacing.
 * 3. When avatars are disabled in discussion settings.
 *
 * @since CW 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function cw_body_class( $classes ) {
	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
		$classes[] = 'sidebar';

	if ( ! get_option( 'show_avatars' ) )
		$classes[] = 'no-avatars';

	return $classes;
}
add_filter( 'body_class', 'cw_body_class' );

/**
 * Register Widget Areas
 *
 * Uncomment and edit to create widget areas where needed.
 * These are default examples so make changes before production.
 *
 * @since CW 1
 *
 */
// function cw_widgets_init() {
// 	register_sidebar( array(
// 		'name'          => __( 'Main Widget Area' ),
// 		'id'            => 'sidebar-1',
// 		'description'   => __( 'Appears in the footer section of the site.' ),
// 		'before_widget' => '<div id="%1$s" class="widget %2$s">',
// 		'after_widget'  => '</div>',
// 		'before_title'  => '<h3 class="widget-title">',
// 		'after_title'   => '</h3>',
// 	) );

// 	register_sidebar( array(
// 		'name'          => __( 'Secondary Widget Area' ),
// 		'id'            => 'sidebar-2',
// 		'description'   => __( 'Appears on posts and pages in the sidebar.' ),
// 		'before_widget' => '<div id="%1$s" class="widget %2$s">',
// 		'after_widget'  => '</div>',
// 		'before_title'  => '<h3 class="widget-title">',
// 		'after_title'   => '</h3>',
// 	) );
// }
// add_action( 'widgets_init', 'cw_widgets_init' );

/**
 * Remove Admin Menu Items
 * http://codex.wordpress.org/Function_Reference/remove_menu_page
 * @since CW 1.0
 */
function cw_remove_admin_menu_items() {
	// remove_menu_page( 'index.php' );                  //Dashboard
	// remove_menu_page( 'edit.php' );                   //Posts
	// remove_menu_page( 'upload.php' );                 //Media
	// remove_menu_page( 'edit.php?post_type=page' );    //Pages
	// remove_menu_page( 'edit-comments.php' );          //Comments
	// remove_menu_page( 'themes.php' );                 //Appearance
	// remove_menu_page( 'plugins.php' );                //Plugins
	// remove_menu_page( 'users.php' );                  //Users
	// remove_menu_page( 'tools.php' );                  //Tools
	// remove_menu_page( 'options-general.php' );        //Settings
}
// add_action('admin_menu', 'cw_remove_admin_menu_items');

function foundation_pagination() {
	global $wp_query;
	$big = 999999999;

	$links = paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'prev_next' => true,
		'prev_text' => '&laquo;',
		'next_text' => '&raquo;',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'type' => 'list'
	)
	);

	$pagination = str_replace('page-numbers','pagination',$links);

	echo $pagination;
}

/**
 * Custom Excerpt Function
 *
 * @link http://www.wpexplorer.com/custom-excerpt-lengths-wordpress/
 * 
 * useage: echo excerpt(25);
 */
function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	} 
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}

function content($limit) {
	$content = explode(' ', get_the_content(), $limit);
	if (count($content)>=$limit) {
		array_pop($content);
		$content = implode(" ",$content).'...';
	} else {
		$content = implode(" ",$content);
	} 
	$content = preg_replace('/\[.+\]/','', $content);
	$content = apply_filters('the_content', $content); 
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}

/**
 * Rename "Posts" to "News"
 *
 * @link http://new2wp.com/snippet/change-wordpress-posts-post-type-news/
 */
// add_action( 'admin_menu', 'pilau_change_post_menu_label' );
// add_action( 'init', 'pilau_change_post_object_label' );
// function pilau_change_post_menu_label() {
// 	global $menu;
// 	global $submenu;
// 	$menu[5][0] = 'News';
// 	$submenu['edit.php'][5][0] = 'News';
// 	$submenu['edit.php'][10][0] = 'Add News';
// 	$submenu['edit.php'][16][0] = 'News Tags';
// 	echo '';
// }
// function pilau_change_post_object_label() {
// 	global $wp_post_types;
// 	$labels = &$wp_post_types['post']->labels;
// 	$labels->name = 'News';
// 	$labels->singular_name = 'News';
// 	$labels->add_new = 'Add News';
// 	$labels->add_new_item = 'Add News';
// 	$labels->edit_item = 'Edit News';
// 	$labels->new_item = 'News';
// 	$labels->view_item = 'View News';
// 	$labels->search_items = 'Search News';
// 	$labels->not_found = 'No News found';
// 	$labels->not_found_in_trash = 'No News found in Trash';
// }


/**
 * Remove "Personal Options" from user profile
 *
 * @link http://wpsnipp.com/index.php/functions-php/remove-personal-options-from-user-profiles/
 */
function hide_personal_options(){
echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) { $(\'form#your-profile > h3:first\').hide(); $(\'form#your-profile > table:first\').hide(); $(\'form#your-profile\').show(); });</script>' . "\n";
}
// add_action('admin_head','hide_personal_options');

// ------------------------------------
//
// Custom Meta Boxes
//
// ------------------------------------
// custom meta boxes
function list_pages() {
	$pages_args = array(
		'sort_order' => 'ASC',
		'sort_column' => 'post_title'
		);

	$pages = get_pages($pages_args);

	global $cw_page_list;
	$cw_page_list = array('' => 'Select a page');
	foreach($pages as $page) {
		$page_name = $page->post_title;
		$page_id = $page->ID;
		$cw_page_list[$page_id] = $page_name;
	}
}
list_pages();

function cw_metaboxes( array $meta_boxes ) {
	global $cw_page_list;
	// use for select, checkbox, radio of list of states
	global $cw_states;

	$prefix = '_cwmb_'; // Prefix for all fields

 	$meta_boxes['slideshows'] = array(
 		'id'         => 'slideshows',
 		'title'      => 'Slideshow Info',
 		'object_types'      => array( 'slideshows' ), // Post type
 		'context'    => 'normal',
 		'priority'   => 'high',
 		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
			    'id'          => $prefix . 'slideshow',
			    'type'        => 'group',
			    'description' => '',
			    'options'     => array(
			        'group_title'   => 'Slide {#}',
			        'add_button'    => 'Add another slide',
			        'remove_button' => 'Remove slide',
			        'sortable'      => true, // beta
			    ),
			    'fields'      => array(
			        array(
			            'name' => 'Title',
			            'id'   => 'title',
			            'type' => 'text',
			        ),
			        array(
			        	'name' => 'Link',
			        	'id' => 'link',
			        	'type' => 'text_url'
			        ),
			        array(
			        	'name' => 'Image',
			        	'id' => 'image',
			        	'type' => 'file'
			        ),
			        array(
			        	'name' => 'Caption',
			        	'id' => 'caption',
			        	'type' => 'textarea'
			        )
			    )
			)
 		)
 	);

 	$meta_boxes['slideshow_page'] = array(
 		'id'         => 'slideshow_page',
 		'title'      => 'Select Page(s)',
 		'object_types'      => array( 'slideshows' ), // Post type
 		'context'    => 'side',
 		'priority'   => 'high',
 		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => '',
				'desc' => 'Choose page(s) to show this slideshow on. NOTE: If no page is selected, this slideshow will note be shown anywhere on the site.',
				'id' => $prefix.'slideshow_page',
				'type' => 'select',
				'options' => $cw_page_list
			)
 		)
 	);

	// promos
	$meta_boxes['promos'] = array(
		'id' => 'promos',
		'title' => 'Promo Info',
		'object_types' => array('promos'), // post type
		// 'show_on' => array( 'key' => 'page-template', 'value' => 'template-contact.php' ), // Limit to page template :)
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Promo Image',
				'id' => $prefix.'promo_image',
				'type' => 'file'
			),
			array(
				'name' => 'Link',
				'id' => $prefix.'promo_link',
				'type' => 'text_url'
			)
		),
	);

	// services
	$meta_boxes['services'] = array(
		'id' => 'services',
		'title' => 'Options',
		'object_types' => array('services'), // post type
		// 'show_on' => array( 'key' => 'page-template', 'value' => 'template-contact.php' ), // Limit to page template :)
		'context' => 'side',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array (
				'name' => 'Service Image',
				'id' => $prefix.'service_image',
				'type' => 'file'
			)
		),
	);

	$meta_boxes['testimonials'] = array(
		'id' => 'testimonials',
		'title' => 'Testimonial',
		'object_types' => array('testimonials'), // post type
		// 'show_on' => array( 'key' => 'page-template', 'value' => 'template-contact.php' ), // Limit to page template :)
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'id' => $prefix.'testimonial',
				'type' => 'textarea'
			),
			array(
				'name' => 'Vocation',
				'id' => $prefix.'vocation',
				'type' => 'text'
			),
			array(
				'name' => 'Location',
				'id' => $prefix.'location',
				'type' => 'text'
			),			
		),
	);

	$meta_boxes['staff'] = array(
		'id' => 'staff',
		'title' => 'Team Member Info',
		'object_types' => array('staff'), // post type
		// 'show_on' => array( 'key' => 'page-template', 'value' => 'template-contact.php' ), // Limit to page template :)
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
			    'name'    => 'Member Type',
			    'id'      => $prefix . 'member_radio',
			    'type'    => 'radio',
			    'options' => array(
			        'board' => __( 'Board Member' ),
			        'other'   => __( 'Other Positions' ),
			    ),
			    'default' => 'board',
			),
			array(
				'name' => 'Title',
				'id' => $prefix.'staff_title',
				'type' => 'text'
			),
			array(
				'name' => 'Name',
				'id' => $prefix.'staff_name',
				'type' => 'text'
			),
			// array(
			// 	'name' => 'Image',
			// 	'id' => $prefix.'staff_image',
			// 	'type' => 'file'
			// ),
			// array(
			// 	'name' => 'Bio',
			// 	'id' => $prefix.'staff_bio',
			// 	'type' => 'textarea'
			// ),			
		),
	);

	//website links ph_links
	$meta_boxes['ph_links'] = array(
		'id' => 'ph_links',
		'title' => 'Website Links',
		'object_types' => array('ph_links'), // post type
		// 'show_on' => array( 'key' => 'page-template', 'value' => 'template-contact.php' ), // Limit to page template :)
		'context' => 'normal',
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			
			array(
				'name' => 'Link URL (Include "http://")',
				'id' => $prefix.'website_link',
				'type' => 'text_url'
			)
		),
	);

	//news link
	$meta_boxes['news_link1'] = array(
		'id' => 'news_link1',
		'title' => 'Article Link',
		'object_types' => array('post'), // post type
		'priority' => 'default',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Link',
				'id' => $prefix.'link',
				'desc' => 'Include (http://)',
				'type' => 'text'
			)
		),
	);

	// example of repeating metaboxes for page template
	// $meta_boxes['careers'] = array(
	// 	'id' => 'careers',
	// 	'title' => 'Career Opportunities',
	// 	'object_types' => array('page'), // post type
	// 	// 'show_on' => array( 'key' => 'page-template', 'value' => 'page-templates/page-template-file-here.php' ), // Limit to page template
	// 	'context' => 'normal',
	// 	'priority' => 'default',
	// 	'show_names' => true, // Show field names on the left
	// 	'fields' => array(
	// 		array(
	// 		    'id'          => $prefix . 'careers',
	// 		    'type'        => 'group',
	// 		    'description' => '',
	// 		    'options'     => array(
	// 		        'group_title'   => 'Tab {#}', // since version 1.1.4, {#} gets replaced by row number
	// 		        'add_button'    => 'Add another item',
	// 		        'remove_button' => 'Remove item',
	// 		        'sortable'      => true, // beta
	// 		    ),
	// 		    // Fields array works the same, except id's only need to be unique for this group. Prefix is not needed.
	// 		    'fields'      => array(
	// 		        array(
	// 		            'name' => 'Career Title',
	// 		            'id'   => 'career_title',
	// 		            'type' => 'text',
	// 		        ),
	// 		        array(
	// 		            'name' => 'Career Content',
	// 		            'id'   => 'career_content',
	// 		            'type' => 'wysiwyg',
	// 		        )
	// 		    ),
	// 		)
	// 	),
	// );

	return $meta_boxes;
}
add_filter( 'cmb2_meta_boxes', 'cw_metaboxes' );

// end custom meta boxes

// get rid of [...]
function new_excerpt_more( $more ) {
	return '&hellip;';
}
add_filter('excerpt_more', 'new_excerpt_more');

// change "enter title here"
// add_filter('gettext','custom_enter_title');
// function custom_enter_title( $input ) {

//     global $post_type;

//     if( is_admin() && 'Enter title here' == $input )
//     	if('testimonials' == $post_type )
//         	return 'Enter Name Here';
//         elseif('staff' == $post_type )
//         	return 'Enter Name Here';

//     return $input;
// }

// block site admin users from creating administrator users
// take from here http://wordpress.stackexchange.com/questions/4479/editor-can-create-any-new-user-except-administrator
class JPB_User_Caps {

	// Add our filters
	function JPB_User_Caps(){
		add_filter( 'editable_roles', array(&$this, 'editable_roles'));
		add_filter( 'map_meta_cap', array(&$this, 'map_meta_cap'),10,4);
	}

	// Remove 'Administrator' from the list of roles if the current user is not an admin
	function editable_roles( $roles ){
		if( isset( $roles['administrator'] ) && !current_user_can('administrator') ){
			unset( $roles['administrator']);
		}
		return $roles;
	}

	// If someone is trying to edit or delete and admin and that user isn't an admin, don't allow it
	function map_meta_cap( $caps, $cap, $user_id, $args ){
		switch( $cap ){
			case 'edit_user':
			case 'remove_user':
			case 'promote_user':
				if( isset($args[0]) && $args[0] == $user_id )
					break;
				elseif( !isset($args[0]) )
					$caps[] = 'do_not_allow';
				$other = new WP_User( absint($args[0]) );
				if( $other->has_cap( 'administrator' ) ){
					if(!current_user_can('administrator')){
						$caps[] = 'do_not_allow';
					}
				}
				break;
			case 'delete_user':
			case 'delete_users':
			    if( !isset($args[0]) )
			        break;
				$other = new WP_User( absint($args[0]) );
			    if( $other->has_cap( 'administrator' ) ){
					if(!current_user_can('administrator')){
						$caps[] = 'do_not_allow';
					}
				}
				break;
			default:
				break;
		}
		return $caps;
	}

}

$jpb_user_caps = new JPB_User_Caps();

// fancy costumizable pagination
// from http://sgwordpress.com/teaches/how-to-add-wordpress-pagination-without-a-plugin/
function pagination($pages = '', $range = 4) {  
	$showitems = ($range * 2)+1;  
	
	global $paged;
	if(empty($paged)) $paged = 1;
	
	if($pages == '') {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages) {
			$pages = 1;
		}
	}   
	
	if(1 != $pages) {
		echo "<div class=\"pagination\">";
		// echo "<span>Page ".$paged." of ".$pages."</span>";
		if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
		if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
		
		for ($i=1; $i <= $pages; $i++) {
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
				echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
			}
		}
		
		if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";  
		if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
		echo "</div>\n";
	}
}

// excerpt size mod
function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


/* Flush rewrite rules for custom post types. */
// flush_rewrite_rules(true);
// global $wp_rewrite; $wp_rewrite->flush_rules();
